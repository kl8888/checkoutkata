﻿using CheckoutKata.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutKata.Classes
{
    public class Items : IItems
    {
        public string SKU { get; set; }

        public decimal UnitPrice { get; set; }

        public int Quantity { get; set; }
    }
}
