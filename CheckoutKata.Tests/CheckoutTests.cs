using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CheckoutKata.Tests
{
    [TestClass]
    public class CheckoutTests
    {
        private CheckoutKata.Classes.Checkout _checkout = new CheckoutKata.Classes.Checkout();

        [TestMethod]
        public void WhenSingleItemProductAReturnUnitPrice()
        {
            var _listOfItems = "A";
            _checkout.Scan(_listOfItems);

            Assert.AreEqual(50, _checkout.GetTotalPrice());
        }

        [TestMethod]
        public void WhenAllSingleItemReturnTotalPrice()
        {
            var _listOfItems = "A,B,C,D";
            _checkout.Scan(_listOfItems);

            Assert.AreEqual(115, _checkout.GetTotalPrice());
        }

        [TestMethod]
        public void WhenMuiltipleSameItemNotOnOfferReturnTotalPrice()
        {
            var _listOfItems = "C,C,C";
            _checkout.Scan(_listOfItems);

            Assert.AreEqual(60, _checkout.GetTotalPrice());
        }

        [TestMethod]
        public void WhenTwoSameItemOnSpecialOfferReturnDiscountedTotalPrice()
        {
            var _listOfItems = "B,B";
            _checkout.Scan(_listOfItems);

            Assert.AreEqual(45, _checkout.GetTotalPrice());
        }

        [TestMethod]
        public void WhenThreeSameItemOnSpecialOfferReturnDiscountedTotalPrice()
        {
            Assert.AreEqual(0, 1);
        }

        [TestMethod]
        public void WhenTwoSameSingleItemsOnSpecialOfferAndSingleItemSameTypeReturnDiscountedTotalPriceForMatchedQuantity()
        {
            Assert.AreEqual(0, 1);
        }

        [TestMethod]
        public void WhenTwoLotOnSpecialOfferReturnDiscountedDoubleDiscountedTotalPrice()
        {
            Assert.AreEqual(0, 1);
        }
    }
}
