﻿using System;
using CheckoutKata.Classes;

namespace CheckoutKata
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter shopping items (CSV format ie A,B,C) : ");

            var _listOfItems = Console.ReadLine().ToUpper();
            var _checkout = new Checkout();
            _checkout.Scan(_listOfItems);

            var _totalPrice = _checkout.GetTotalPrice();

            Console.WriteLine("Total Price : " + _totalPrice);
            Console.ReadKey();
        }
    }
}
