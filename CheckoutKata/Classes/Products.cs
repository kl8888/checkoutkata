﻿using CheckoutKata.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutKata.Classes
{
    public class Products : IProducts
    {
        public string SKU { get; set; }

        public decimal UnitPrice { get; set; }
    }
}
