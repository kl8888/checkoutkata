﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutKata.Classes.Interfaces
{
    public interface ICheckout
    {
        public void Scan();
        public decimal GetTotalPrice();
    }
}
