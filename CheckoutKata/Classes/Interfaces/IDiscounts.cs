﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutKata.Classes.Interfaces
{
    public interface IDiscounts
    {
        string SKU { get; set; }

        decimal SpecialPrice { get; set; }

        int Quantity { get; set; }
    }
}
