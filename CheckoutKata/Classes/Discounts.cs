﻿using System;
using System.Collections.Generic;
using System.Text;
using CheckoutKata.Classes.Interfaces;

namespace CheckoutKata.Classes
{
    public class Discounts : IDiscounts
    {
        public string SKU { get; set; }

        public decimal SpecialPrice { get; set; }

        public int Quantity { get; set; }
    }
}
