﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CheckoutKata.Classes.Interfaces
{
    public interface IProducts
    {
        string SKU { get; set; }

        decimal UnitPrice { get; set; }
    }
}
