﻿using CheckoutKata.Classes.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CheckoutKata.Classes
{
    public class Checkout
    {
        private List<IItems> _shoppingItems = new List<IItems>();
        private List<IProducts> _listProducts = new List<IProducts>();
        private List<IDiscounts> _discount = new List<IDiscounts>();

        public Checkout()
        {
            // No database, setup products and discounts, alternatively, could have used JSON/XML file to hold data
            // Information provided from table 
            _listProducts = new List<IProducts>() {
                new Products(){ SKU = "A", UnitPrice = 50},
                new Products(){ SKU = "B", UnitPrice = 30},
                new Products(){ SKU = "C", UnitPrice = 20},
                new Products(){ SKU = "D", UnitPrice = 15},
            };

            _discount = new List<IDiscounts>()
            {
                new Discounts(){ SKU = "A", SpecialPrice = 130, Quantity = 3},
                new Discounts(){ SKU = "B", SpecialPrice = 45, Quantity = 2},
            };
        }

        public void Scan(string ListOfItems)
        {
            var _list = ListOfItems.Split(",").ToList();

            // Loop input and add to shopping list
            foreach (var sku in _list)
            {
                var _item = new Items();

                // Test if SKU from input exist, if not, create item and add to list
                if (!_shoppingItems.Any(d => d.SKU == sku))
                {
                    _item.SKU = sku;
                    _item.UnitPrice = _listProducts.Where(p => p.SKU == sku).Select(p => p.UnitPrice).FirstOrDefault();
                    _item.Quantity = 1;
                    _shoppingItems.Add(_item);
                }
                else
                { // if SKU exist, increment quantity
                    foreach (var item in _shoppingItems)
                    {
                        if (sku == item.SKU)
                        {
                            item.Quantity += 1;
                        }
                    }
                }
            }
        }

        public decimal GetTotalPrice()
        {
            var _total = 0m;

            // Loop shopping list and total
            foreach (var item in _shoppingItems)
            {
                // Test to see if item is on special offer, by matching SKU and Qty
                if (_discount.Any(d => d.SKU == item.SKU && d.Quantity == item.Quantity))
                {
                    _total += _discount.Where(d => d.SKU == item.SKU).Select(r => r.SpecialPrice).FirstOrDefault();                    
                }
                else
                {
                    _total += item.UnitPrice * item.Quantity;
                }
            }

            return _total;
        }

    }
}
